import java.util.Scanner;
public class lab1 {
    
    public static void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public static void showTable(char[][] table){
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    public static void showTurn(char turn){
        System.out.println("Turn "+turn);
    }
   
    public static void input(char[][] table,char turn){
        int row,col;
        Scanner input = new Scanner(System.in);
        showTurn(turn);
        while(true){
            System.out.print("Please input Row Col: ");
            row = input.nextInt();
            col = input.nextInt();
            if(row > 0 && row < 4 && col > 0 && col < 4){
                if(table[row-1][col-1] != 'O' && table[row-1][col-1] != 'X'){
                    break;
                }
            }
        }
        table[row-1][col-1] = turn;        
    }
    
    public static String checkHorizontal(char[][] table){
        String sum = "";
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                sum += table[i][j];
            }
            if(sum.equals("OOO") || sum.equals("XXX"))
                return sum;
            else
                sum = "";
        }
        return sum;
    }
        
    public static String checkVertical(char[][] table){
        String sum = "";
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++)
                sum += table[j][i];
            if(sum.equals("OOO") || sum.equals("XXX"))
                return sum;
            else
                sum = "";
        }
        return sum;
    }
    
    public static boolean checkTile(char[][] table){
        if(table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'){
                return true;
            }else if(table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'){
                return true;
            }
        return false;
    }

    public static boolean checkTile2(char[][] table){
        if(table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O'){
                return true;
            }else if(table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X'){
                return true;
            }
        return false;
    }
    
    public static boolean checkWin(char[][] table){
        if(checkVertical(table).equals("OOO") || checkVertical(table).equals("XXX")){
            return true;
        }else if(checkHorizontal(table).equals("OOO") || checkHorizontal(table).equals("XXX")){
            return true;
        }else if(checkTile(table)){
            return true;
        }else if(checkTile2(table)){
            return true;
        }
        return false;
    }
    public static char switchPlayer(char turn){
        if(turn == 'O'){
            return 'X';
        }else{
            return 'O';
        }
    }
    public static void showDraw(char[][] table){
        showTable(table);
        System.out.println("Draw");
        showByebye();
    }
    
    public static void showWin(char[][] table, char turn){
        showTable(table);
        showPlayerwin(turn);
        showByebye();
    }
    public static void showPlayerwin(char turn){
        System.out.println("Player "+turn+" Win");
    }
    public static void showByebye (){
        System.out.println("Bye Bye . . .");
    }
    
    public static void main (String[] args){
        char[][] arr = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
        char turn = 'O';
        int round = 0;
        showWelcome();
        while(true){
            showTable(arr);
            input(arr,turn);
            if(round==8){
                showDraw(arr);break;
            }
            if(checkWin(arr)){
                showWin(arr,turn);break;
            }else{
                turn = switchPlayer(turn);
                round++;
            }
        }
    }
}
